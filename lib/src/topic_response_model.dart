part of '../d2d_notifications.dart';

class D2DNotificationTopicResponse {
  String? messageId;

  D2DNotificationTopicResponse({this.messageId});

  D2DNotificationTopicResponse.fromJson(Map<String, dynamic> json) {
    messageId = json['message_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['message_id'] = messageId;
    return data;
  }
}