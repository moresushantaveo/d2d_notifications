part of '../d2d_notifications.dart';

class D2DNotificationRequest {
  String to;
  String title;
  String subtitle;
  String body;
  bool mutableContent;
  String sound;
  String deeplink;
  String imageUrl;

  D2DNotificationRequest({
    required this.to,
    required this.title,
    this.subtitle = "",
    this.body = "",
    this.mutableContent = true,
    this.sound = "Tri-tone",
    this.deeplink = "",
    this.imageUrl = "",
  });
}