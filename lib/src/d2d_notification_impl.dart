part of '../d2d_notifications.dart';

class D2DNotification {
  late String serverKey;

  D2DNotification._internal();
  static final D2DNotification instance = D2DNotification._internal();

  factory D2DNotification({required String serverKey}) {
    instance.serverKey = serverKey;
    return instance;
  }

  Future<D2DNotificationTokenResponse?> sendNotificationToToken(
      D2DNotificationRequest request) async {
    Map<String, String> requestHeaders = {
      "Authorization": "key=$serverKey",
      "Content-Type": "application/json"
    };
    try {
      http.Response response =
          await http.post(Uri.parse("https://fcm.googleapis.com/fcm/send"),
              body: jsonEncode(
                {
                  "to": request.to,
                  "notification": {
                    "title": request.title,
                    "subtitle": request.subtitle,
                    "body": request.body,
                    "mutable_content": request.mutableContent,
                    "sound": request.sound,
                    "image": request.imageUrl,
                  },
                  "data": {
                    "deeplink": request.deeplink,
                  }
                },
              ),
              headers: requestHeaders);
      if (response.reasonPhrase == "OK") {
        return D2DNotificationTokenResponse.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<D2DNotificationTopicResponse?> sendNotificationToTopic(
      D2DNotificationRequest request) async {
    Map<String, String> requestHeaders = {
      "Authorization": "key=$serverKey",
      "Content-Type": "application/json"
    };
    try {
      http.Response response =
          await http.post(Uri.parse("https://fcm.googleapis.com/fcm/send"),
              body: jsonEncode(
                {
                  "to": "/topics/${request.to}",
                  "notification": {
                    "title": request.title,
                    "subtitle": request.subtitle,
                    "body": request.body,
                    "mutable_content": request.mutableContent,
                    "sound": request.sound,
                    "image": request.imageUrl,
                  },
                  "data": {
                    "deeplink": request.deeplink,
                  }
                },
              ),
              headers: requestHeaders);
      if (response.reasonPhrase == "OK") {
        return D2DNotificationTopicResponse.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
