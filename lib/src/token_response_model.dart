part of '../d2d_notifications.dart';

class D2DNotificationTokenResponse {
  int? multicastId;
  int? success;
  int? failure;
  int? canonicalIds;
  List<Results>? results;

  D2DNotificationTokenResponse({
    this.multicastId,
    this.success,
    this.failure,
    this.canonicalIds,
    this.results,
  });

  D2DNotificationTokenResponse.fromJson(Map<String, dynamic> json) {
    multicastId = json['multicast_id'];
    success = json['success'];
    failure = json['failure'];
    canonicalIds = json['canonical_ids'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['multicast_id'] = multicastId;
    data['success'] = success;
    data['failure'] = failure;
    data['canonical_ids'] = canonicalIds;
    if (results != null) {
      data['results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  String? messageId;

  Results({this.messageId});

  Results.fromJson(Map<String, dynamic> json) {
    messageId = json['message_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['message_id'] = messageId;
    return data;
  }
}