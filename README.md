

# D2D Notifications

Using Firebase Cloud for entire application and no Custom Server?
Ever wanted to send a notification from client side (user app)?
D2D Notification is a one stop solution to trigger / send FCM Push Notification, from one device to another device (D2D), without any server.

## Platform Support
This plugin is only used to trigger / send a notification request over HTTP. It does not handle any actions regarding the Firebase Messaging (receiving push notification).

|  		  | Android | iOS | MacOS | Web | Linux | Windows |
| :----:  | :-----: | :-: | :---: | :-: | :---: | :-----: |
| Send  from   |   ✔️    | ✔️  |  ✔️   | ✔️  |  ✔️   |   ✔️    |



## Usage
To use this plugin, add `d2d_notifications` as a [dependency in your pubspec.yaml file](https://flutter.dev/docs/development/platform-integration/platform-channels).

#### Initialise the d2d_notification service by providing [serverKey](https://documentation.onesignal.com/docs/generate-a-google-server-api-key).
```dart  
void main() {
	WidgetsFlutterBinding.ensureInitialized();
	D2DNotification(serverKey: "YOUR_SERVER_KEY"); // Add this line in main().
	runApp(const MyApp());
}
```  

#### Generate request parameters for notification.

Parameter "**to**" can include either a [TOPIC](https://firebase.google.com/docs/cloud-messaging/send-message#send-messages-to-topics-legacy) or [FCM TOKEN](https://firebase.google.com/docs/cloud-messaging/send-message#send-messages-to-specific-devices-legacy)

Firebase provides us a concept of Topics. Every subscriber of that particular topic, will be able to receive notification send for that particular topic.

**Topic Example**
Consider a user is subscribed to a topic named "**dogs**"
```dart
D2DNotificationRequest request = D2DNotificationRequest(  
  to: "dogs",
  title: "This is title",
  subtitle: "This is subtitle",
  body: "This is body",
  imageUrl: "https://picsum.photos/200/300",
  deeplink: "YOUR_DEEPLINK",
);  
// Send notification request & receive response.
D2DNotificationTopicResponse? response = await D2DNotification.instance.sendNotificationToTopic(request);  
if(response != null) {  
  print("Notification sent successfully. ${response.messageId}");  
}
```

On every fresh install of your application, firebase generates a FCM token that is specific to that particular instance of app in user's device. To target a single user, you may use this. You will need the FCM Token for that user's application instance.

**Token Example**
Consider a user has FCM token as "**xyz**"
```dart
D2DNotificationRequest request = D2DNotificationRequest(  
  to: "xyz",
  title: "This is title",
  subtitle: "This is subtitle",
  body: "This is body",
  imageUrl: "https://picsum.photos/200/300",
  deeplink: "YOUR_DEEPLINK",
);  
// Send notification request & receive response.
D2DNotificationTokenResponse? response = await D2DNotification.instance.sendNotificationToToken(request);  
if(response != null) {  
  print("Notification sent successfully. ${response.results}");  
}
```


## Additional information

This package is developed and maintained by [AveoSoft Pvt Ltd](https://aveosoft.com/).
For any issues & improvements you can create an issue in Github Issues.